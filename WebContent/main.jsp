<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>共享租车管理系统主界面</title>
<%
	// 权限验证
	if(session.getAttribute("currentUser")==null){
		response.sendRedirect("index.html");
		return;
	}
%>
<link rel="stylesheet" type="text/css" href="jquery-easyui-1.3.3/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="jquery-easyui-1.3.3/themes/icon.css">
<script type="text/javascript" src="jquery-easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript" src="jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
	$(function(){
		// 数据
		var treeData=[{
			text:"共享租车",
			children:[
{
	text:"汽车账户部管理",
	attributes:
	{
		url:"companyInfoManage.jsp"
	}
 },
{
	text:"租车账户部",
	attributes:
	{
		url:"consumerinfoManage.jsp"
	}
 },

			{
				text:"汽车信息管理表",
				attributes:
				{
					url:"taxicarInfoManage.jsp"
				}
			 },
			 {
					text:"租车记录表",
					attributes:
					{
						url:"electriccarInfoManage.jsp"
					}
				 },
			
			{
				text:"租车租凭表",
				attributes:
				{
					url:"customerInfoManage.jsp"
				}
			 }
			]
		}];
		
		// 实例化树菜单
		$("#tree").tree({
			data:treeData,
			lines:true,
			onClick:function(node){
				if(node.attributes){
					openTab(node.text,node.attributes.url);
				}
			}
		});
		
		// 新增Tab
		function openTab(text,url){
			if($("#tabs").tabs('exists',text)){
				$("#tabs").tabs('select',text);
			}else{
				var content="<iframe frameborder='0' scrolling='auto' style='width:100%;height:100%' src="+url+"></iframe>";
				$("#tabs").tabs('add',{
					title:text,
					closable:true,
					content:content
				});
			}
		}
	});
</script>
</head>
<body class="easyui-layout">
	<div region="north" style="height: 80px;background-color: #E0EDFF">
		<div align="left" style="width: 80%;float: left"><img src="images/2.2.jpg"></div>
		<div style="padding-top: 40px;padding-right: 20px;">当前用户：&nbsp;<font color="red" >${currentUser.userName }</font></div>
		<a href="loginout">当前用户注销</a>
	</div>
	<div region="center">
		<div class="easyui-tabs" fit="true" border="false" id="tabs">
			<div title="首页" >
				<!-- <div align="left" style="padding-top: 38px;"><font color="blue" size="6.5">XX师范学院</font></div> -->
				<div align="left" style="padding-top: 28px;"><font color="blue" size="5">欢迎使用共享租车信息管理系统</font></div>
				<div align="left" style="padding-top: 10px;"><font color="blue" size="3">开发人员：Instinct</font></div>
				<div align="left" style="padding-top: 1px;"><font color="blue" size="3">开发周期：2018---2019</font></div>
				<div align="left" style="padding-top: 4px;"><font color="blue" size="1">----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</font></div>
			    <div align="left" style="padding-top: 4px;"><font color="blue" size="4">系统环境</font></div>
                <div align="left" style="padding-top: 4px;"><font color="blue" size="2">系统环境：Windows</font></div>
                <div align="left" style="padding-top: 4px;"><font color="blue" size="2">开发工具：Ecipse</font></div>
                <div align="left" style="padding-top: 4px;"><font color="blue" size="2">Java版本：JDK 1.8</font></div>
                <div align="left" style="padding-top: 4px;"><font color="blue" size="2">服务器：tomcat8.0</font></div>
                <div align="left" style="padding-top: 4px;"><font color="blue" size="2">数据库：MYSQL5.5</font></div>
                <div align="left" style="padding-top: 4px;"><font color="blue" size="2">系统采用技术：Servlet+Jsp+Jdbc+dbutls+EasyUI+JQuery+Ajax+面向接口编程</font></div>
            </div>
		</div>
	</div>
	<div region="west" style="width: 175px;" title="导航菜单" split="true">
		<ul id="tree"></ul>
	</div>
	<div region="south" style="height: 25px;" align="center">版权所有<a href="">www.lingnanedu.com</a></div>
</body>
</html>