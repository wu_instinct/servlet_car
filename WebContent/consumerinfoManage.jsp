<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>租车账户部</title>
<link rel="stylesheet" type="text/css" href="jquery-easyui-1.3.3/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="jquery-easyui-1.3.3/themes/icon.css">
<script type="text/javascript" src="jquery-easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript" src="jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
	var url;
	
	function searchConsumer(){
		$('#dg').datagrid('load',{
			consumerName:$('#s_consumerName').val()
		});
	}
	
	function deleteConsumer(){
		var selectedRows=$("#dg").datagrid('getSelections');
		if(selectedRows.length==0){
			$.messager.alert("系统提示","请选择要删除的数据！");
			return;
		}
		var strIds=[];
		for(var i=0;i<selectedRows.length;i++){
			strIds.push(selectedRows[i].id);
		}
		var ids=strIds.join(",");
		$.messager.confirm("系统提示","您确认要删掉这<font color=red>"+selectedRows.length+"</font>条数据吗？",function(r){
			if(r){
				$.post("consumerDelete",{delIds:ids},function(result){
					if(result.success){
						$.messager.alert("系统提示","您已成功删除<font color=red>"+result.delNums+"</font>条数据！");
						$("#dg").datagrid("reload");
					}else{
						$.messager.alert('系统提示','<font color=red>'+selectedRows[result.errorIndex].companyName+'</font>'+result.errorMsg);
					}
				},"json");
			}
		});
	}
	
	
 	function openConsumerAddDialog(){
		$("#dlg").dialog("open").dialog("setTitle","添加用户信息");
		url="consumerSave";
	} 
	
	function openConsumerModifyDialog(){
		var selectedRows=$("#dg").datagrid('getSelections');
		if(selectedRows.length!=1){
			$.messager.alert("系统提示","请选择一个要审核权限的用户！");
			return;
		}
		var row=selectedRows[0];
		$("#dlg").dialog("open").dialog("setTitle","审核用户权限");
		$("#fm").form("load",row);
		url="consumerSave?id="+row.id;
	}
	
	function closeConsumerDialog(){
		$("#dlg").dialog("close");
		resetValue();
	}
	
	function resetValue(){
		$("#consumerName").val("");
		$("#password").val("");
	}
	
	
	function saveConsumer(){
		$("#fm").form("submit",{
			url:url,
			onSubmit:function(){
				return $(this).form("validate");
			},
			success:function(result){
				if(result.errorMsg){
					$.messager.alert("系统提示",result.errorMsg);
					return;
				}else{
					$.messager.alert("系统提示","保存成功");
					resetValue();
					$("#dlg").dialog("close");
					$("#dg").datagrid("reload");
				}
			}
		});
	}
	
	
</script>
</head>
<body style="margin: 5px;">
	<table id="dg" title="用户信息" class="easyui-datagrid" fitColumns="true"
	 pagination="true" rownumbers="true" url="consumerList" fit="true" toolbar="#tb">
		<thead>
			<tr>
				<th field="cb" checkbox="true"></th>
				<th field="id" width="50">用户ID</th>
				<th field="consumerName" width="100">用户(驾龄)</th>
				<th field="password" width="250">租车权限</th>
			</tr>
		</thead>
	</table>
	
	<div id="tb">
		 <div>
		   <a href="javascript:openConsumerAddDialog()" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
 		 <a href="javascript:deleteConsumer()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
		 <a href="javascript:openConsumerModifyDialog()" class="easyui-linkbutton" iconCls="icon-edit" plain="true" ><font color="red">审核租车权限</font></a>
    	 </div>
		<div>&nbsp;用户(驾龄)：&nbsp;<input type="text" name="s_consumerName" id="s_consumerName"/><a href="javascript:searchConsumer()" class="easyui-linkbutton" iconCls="icon-search" plain="true">搜索</a></div>
	</div>
	
	<div id="dlg" class="easyui-dialog" style="width: 400px;height: 280px;padding: 10px 20px"
		closed="true" buttons="#dlg-buttons">
		<form id="fm" method="post">
			<table>
				<tr>
					<td>用户(驾龄)：</td>
					<td><input type="text" name="consumerName" id="consumerName" class="easyui-validatebox" required="true"/></td>
				</tr>
				<tr>
					<td valign="top">租车权限：</td>
					<td><textarea name="password" id="password" ></textarea></td>
				</tr>
			</table>
		</form>
	</div>
	
	<div id="dlg-buttons">
		<a href="javascript:saveConsumer()" class="easyui-linkbutton" iconCls="icon-ok"><font color="red">审核</font></a>
		<a href="javascript:closeConsumerDialog()" class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
	</div>
</body>
</html>