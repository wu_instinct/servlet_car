package cn.edu.lingnan.model;

public class Company {

	private int id;
	private String companyName;
	private String companyDesc;
	
	
	public Company() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Company(String gradeName, String gradeDesc) {
		super();
		this.companyName = gradeName;
		this.companyDesc = gradeDesc;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getCompanyDesc() {
		return companyDesc;
	}


	public void setCompanyDesc(String companyDesc) {
		this.companyDesc = companyDesc;
	}

	
}
