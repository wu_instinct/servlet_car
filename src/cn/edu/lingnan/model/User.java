package cn.edu.lingnan.model;

/**
 * �û�Model��
 * @author www.java1234.com
 *
 */
public class User {

	private String id;
	private String userName;
	private String password;
	private String sspur;
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSspur() {
		return sspur;
	}
	public void setSspur(String sspur) {
		this.sspur = sspur;
	}
	
}
