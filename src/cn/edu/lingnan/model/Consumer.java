package cn.edu.lingnan.model;

public class Consumer {

	private int id;
	private String consumerName;
	private String password;
	
	
	public Consumer() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Consumer(String consumerName, String password) {
		super();
		this.consumerName = consumerName;
		this.password = password;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getConsumerName() {
		return consumerName;
	}


	public void setConsumerName(String consumerName) {
		this.consumerName = consumerName;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
}
