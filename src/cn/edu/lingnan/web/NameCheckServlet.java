package cn.edu.lingnan.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.edu.lingnan.dao.registDao;

public class NameCheckServlet extends HttpServlet {

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
      String username =req.getParameter("username");
      registDao sd=new registDao();
      boolean flag = false;
	try {
		flag = sd.findByName(username);
	} catch (ClassNotFoundException e) {
		e.printStackTrace();
	}
      if(flag)
    	  resp.getWriter().print("true");
      else
    	  resp.getWriter().print("flase");
        
	}
}
