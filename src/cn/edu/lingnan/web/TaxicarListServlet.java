package cn.edu.lingnan.web;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import cn.edu.lingnan.dao.TaxicarDao;
import cn.edu.lingnan.model.Customer;
import cn.edu.lingnan.model.PageBean;
import cn.edu.lingnan.model.Taxicar;
import cn.edu.lingnan.util.DbUtil;
import cn.edu.lingnan.util.JsonUtil;
import cn.edu.lingnan.util.ResponseUtil;
import cn.edu.lingnan.util.StringUtil;

public class TaxicarListServlet extends HttpServlet{
	DbUtil dbUtil=new DbUtil();
	TaxicarDao taxicarDao=new TaxicarDao();
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String stuNo=request.getParameter("stuNo");
		String stuName=request.getParameter("stuName");
		String sex=request.getParameter("sex");
		String bbirthday=request.getParameter("bbirthday");
		String ebirthday=request.getParameter("ebirthday");
		String consumerId=request.getParameter("consumerId");
		
		Taxicar taxicar=new Taxicar();
		if(stuNo!=null){
			taxicar.setStuNo(stuNo);
			taxicar.setStuName(stuName);
			taxicar.setSex(sex);
			if(StringUtil.isNotEmpty(consumerId)){
				taxicar.setConsumerId(Integer.parseInt(consumerId));
			}
		}
		
		String page=request.getParameter("page");
		String rows=request.getParameter("rows");
	
		PageBean pageBean=new PageBean(Integer.parseInt(page),Integer.parseInt(rows));
		Connection con=null;
		try{
			con=dbUtil.getCon();
			JSONObject result=new JSONObject();
			JSONArray jsonArray=JsonUtil.formatRsToJsonArray(taxicarDao.taxicarList(con, pageBean,taxicar,bbirthday,ebirthday));
			int total=taxicarDao.taxicarCount(con,taxicar,bbirthday,ebirthday);
			result.put("rows", jsonArray);
			result.put("total", total);
			ResponseUtil.write(response, result);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				dbUtil.closeCon(con);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
	
}
