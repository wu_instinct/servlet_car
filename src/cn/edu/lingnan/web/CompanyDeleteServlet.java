package cn.edu.lingnan.web;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;
import cn.edu.lingnan.dao.CompanyDao;
import cn.edu.lingnan.dao.CustomerDao;
import cn.edu.lingnan.util.DbUtil;
import cn.edu.lingnan.util.ResponseUtil;

public class CompanyDeleteServlet extends HttpServlet{
	DbUtil dbUtil=new DbUtil();
	CompanyDao companyDao=new CompanyDao();
	CustomerDao customerDao=new CustomerDao();
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String delIds=request.getParameter("delIds");
		Connection con=null;
		try{
			con=dbUtil.getCon();
			JSONObject result=new JSONObject();
			String str[]=delIds.split(",");
			for(int i=0;i<str.length;i++){
				boolean f=customerDao.getCustomerByCompanyId(con, str[i]);
				if(f){
					result.put("errorIndex", i);
					result.put("errorMsg", "公司下面有租车用户，不能删除！");
					ResponseUtil.write(response, result);
					return;
				}
			}
			int delNums=companyDao.companyDelete(con, delIds);
			if(delNums>0){
				result.put("success", "true");
				result.put("delNums", delNums);
			}else{
				result.put("errorMsg", "删除失败");
			}
			ResponseUtil.write(response, result);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				dbUtil.closeCon(con);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}	
}
