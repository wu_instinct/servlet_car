package cn.edu.lingnan.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import cn.edu.lingnan.model.Electriccar;
import cn.edu.lingnan.model.PageBean;
import cn.edu.lingnan.util.DateUtil;
import cn.edu.lingnan.util.StringUtil;

public class ElectriccarDao {

	public ResultSet electriccarList(Connection con,PageBean pageBean,Electriccar electriccar,String bbirthday,String ebirthday)throws Exception{
		StringBuffer sb=new StringBuffer("select * from t_electriccar s,t_consumer g where s.consumerId=g.id");
		if(StringUtil.isNotEmpty(electriccar.getStuNo())){
			sb.append(" and s.stuNo like '%"+electriccar.getStuNo()+"%'");
		}
		if(StringUtil.isNotEmpty(electriccar.getStuName())){
			sb.append(" and s.stuName like '%"+electriccar.getStuName()+"%'");
		}
		if(StringUtil.isNotEmpty(electriccar.getSex())){
			sb.append(" and s.sex ='"+electriccar.getSex()+"'");
		}
		if(electriccar.getConsumerId()!=-1){
			sb.append(" and s.consumerId ='"+electriccar.getConsumerId()+"'");
		}
		if(StringUtil.isNotEmpty(bbirthday)){
			sb.append(" and TO_DAYS(s.birthday)>=TO_DAYS('"+bbirthday+"')");
		}
		if(StringUtil.isNotEmpty(ebirthday)){
			sb.append(" and TO_DAYS(s.birthday)<=TO_DAYS('"+ebirthday+"')");
		}
		if(pageBean!=null){
			sb.append(" limit "+pageBean.getStart()+","+pageBean.getRows());
		}
		PreparedStatement pstmt=con.prepareStatement(sb.toString());
		return pstmt.executeQuery();
	}
	
	public int electriccarCount(Connection con,Electriccar electriccar,String bbirthday,String ebirthday)throws Exception{
		StringBuffer sb=new StringBuffer("select count(*) as total from t_electriccar s,t_consumer g where s.consumerId=g.id");
		if(StringUtil.isNotEmpty(electriccar.getStuNo())){
			sb.append(" and s.stuNo like '%"+electriccar.getStuNo()+"%'");
		}
		if(StringUtil.isNotEmpty(electriccar.getStuName())){
			sb.append(" and s.stuName like '%"+electriccar.getStuName()+"%'");
		}
		if(StringUtil.isNotEmpty(electriccar.getSex())){
			sb.append(" and s.sex ='"+electriccar.getSex()+"'");
		}
		if(electriccar.getConsumerId()!=-1){
			sb.append(" and s.consumerId ='"+electriccar.getConsumerId()+"'");
		}
		if(StringUtil.isNotEmpty(bbirthday)){
			sb.append(" and TO_DAYS(s.birthday)>=TO_DAYS('"+bbirthday+"')");
		}
		if(StringUtil.isNotEmpty(ebirthday)){
			sb.append(" and TO_DAYS(s.birthday)<=TO_DAYS('"+ebirthday+"')");
		}
		PreparedStatement pstmt=con.prepareStatement(sb.toString());
		ResultSet rs=pstmt.executeQuery();
		if(rs.next()){
			return rs.getInt("total");
		}else{
			return 0;
		}
	}
	
	public int electriccarDelete(Connection con,String delIds)throws Exception{
		String sql="delete from t_electriccar where stuId in("+delIds+")";
		PreparedStatement pstmt=con.prepareStatement(sql);
		return pstmt.executeUpdate();
	}
	
	public int electriccarAdd(Connection con,Electriccar electriccar)throws Exception{
		String sql="insert into t_electriccar values(null,?,?,?,?,?,?,?)";
		PreparedStatement pstmt=con.prepareStatement(sql);
		pstmt.setString(1, electriccar.getStuNo());
		pstmt.setString(2, electriccar.getStuName());
		pstmt.setString(3, electriccar.getSex());
		pstmt.setString(4, DateUtil.formatDate(electriccar.getBirthday(), "yyyy-MM-dd"));
		pstmt.setInt(5, electriccar.getConsumerId());
		pstmt.setString(6, electriccar.getEmail());
		pstmt.setString(7, electriccar.getStuDesc());
		return pstmt.executeUpdate();
	}
	
	public int electriccarModify(Connection con,Electriccar electriccar)throws Exception{
		String sql="update t_electriccar set stuNo=?,stuName=?,sex=?,birthday=?,consumerId=?,email=?,stuDesc=? where stuId=?";
		PreparedStatement pstmt=con.prepareStatement(sql);
		pstmt.setString(1, electriccar.getStuNo());
		pstmt.setString(2, electriccar.getStuName());
		pstmt.setString(3, electriccar.getSex());
		pstmt.setString(4, DateUtil.formatDate(electriccar.getBirthday(), "yyyy-MM-dd"));
		pstmt.setInt(5, electriccar.getConsumerId());
		pstmt.setString(6, electriccar.getEmail());
		pstmt.setString(7, electriccar.getStuDesc());
		pstmt.setInt(8, electriccar.getStuId());
		return pstmt.executeUpdate();
	}
	
	public boolean getElectriccarByconsumerId(Connection con,String consumerId)throws Exception{
		String sql="select * from t_electriccar where consumerId=?";
		PreparedStatement pstmt=con.prepareStatement(sql);
		pstmt.setString(1, consumerId);
		ResultSet rs=pstmt.executeQuery();
		if(rs.next()){
			return true;
		}else{
			return false;
		}
	}
}
