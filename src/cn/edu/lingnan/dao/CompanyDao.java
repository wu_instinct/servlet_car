

package cn.edu.lingnan.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import cn.edu.lingnan.model.Company;
import cn.edu.lingnan.model.PageBean;
import cn.edu.lingnan.util.StringUtil;

public class CompanyDao {

	public ResultSet companyList(Connection con,PageBean pageBean,Company company)throws Exception{
		StringBuffer sb=new StringBuffer("select * from t_company");
		if(company!=null && StringUtil.isNotEmpty(company.getCompanyName())){
			sb.append(" and companyName like '%"+company.getCompanyName()+"%'");
		}
		if(pageBean!=null){
			sb.append(" limit "+pageBean.getStart()+","+pageBean.getRows());
		}
		PreparedStatement pstmt=con.prepareStatement(sb.toString().replaceFirst("and", "where"));
		return pstmt.executeQuery();
	}
	
	public int companyCount(Connection con,Company company)throws Exception{
		StringBuffer sb=new StringBuffer("select count(*) as total from t_company");
		if(StringUtil.isNotEmpty(company.getCompanyName())){
			sb.append(" and companyName like '%"+company.getCompanyName()+"%'");
		}
		PreparedStatement pstmt=con.prepareStatement(sb.toString().replaceFirst("and", "where"));
		ResultSet rs=pstmt.executeQuery();
		if(rs.next()){
			return rs.getInt("total");
		}else{
			return 0;
		}
	}
	
	/**
	 * 
	 * delete from tableName where field in (1,3,5)
	 * @param con
	 * @param delIds
	 * @return
	 * @throws Exception
	 */
	public int companyDelete(Connection con,String delIds)throws Exception{
		String sql="delete from t_company where id in("+delIds+")";
		PreparedStatement pstmt=con.prepareStatement(sql);
		return pstmt.executeUpdate();
	}
	
	public int companyAdd(Connection con,Company company)throws Exception{
		String sql="insert into t_company values(null,?,?)";
		PreparedStatement pstmt=con.prepareStatement(sql);
		pstmt.setString(1, company.getCompanyName());
		pstmt.setString(2, company.getCompanyDesc());
		return pstmt.executeUpdate();
	}
	
	public int companyModify(Connection con,Company company)throws Exception{
		String sql="update t_company set companyName=?,companyDesc=? where id=?";
		PreparedStatement pstmt=con.prepareStatement(sql);
		pstmt.setString(1, company.getCompanyName());
		pstmt.setString(2, company.getCompanyDesc());
		pstmt.setInt(3, company.getId());
		return pstmt.executeUpdate();
	}

	
}
