package cn.edu.lingnan.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import cn.edu.lingnan.model.Customer;
import cn.edu.lingnan.model.PageBean;
import cn.edu.lingnan.util.DateUtil;
import cn.edu.lingnan.util.StringUtil;

public class CustomerDao {

	public ResultSet customerList(Connection con,PageBean pageBean,Customer customer,String bbirthday,String ebirthday)throws Exception{
		StringBuffer sb=new StringBuffer("select * from t_customer s,t_company g where s.companyId=g.id");
		if(StringUtil.isNotEmpty(customer.getStuNo())){
			sb.append(" and s.stuNo like '%"+customer.getStuNo()+"%'");
		}
		if(StringUtil.isNotEmpty(customer.getStuName())){
			sb.append(" and s.stuName like '%"+customer.getStuName()+"%'");
		}
		if(StringUtil.isNotEmpty(customer.getSex())){
			sb.append(" and s.sex ='"+customer.getSex()+"'");
		}
		if(customer.getCompanyId()!=-1){
			sb.append(" and s.companyId ='"+customer.getCompanyId()+"'");
		}
		if(StringUtil.isNotEmpty(bbirthday)){
			sb.append(" and TO_DAYS(s.birthday)>=TO_DAYS('"+bbirthday+"')");
		}
		if(StringUtil.isNotEmpty(ebirthday)){
			sb.append(" and TO_DAYS(s.birthday)<=TO_DAYS('"+ebirthday+"')");
		}
		if(pageBean!=null){
			sb.append(" limit "+pageBean.getStart()+","+pageBean.getRows());
		}
		PreparedStatement pstmt=con.prepareStatement(sb.toString());
		return pstmt.executeQuery();
	}
	
	public int customerCount(Connection con,Customer customer,String bbirthday,String ebirthday)throws Exception{
		StringBuffer sb=new StringBuffer("select count(*) as total from t_customer s,t_company g where s.companyId=g.id");
		if(StringUtil.isNotEmpty(customer.getStuNo())){
			sb.append(" and s.stuNo like '%"+customer.getStuNo()+"%'");
		}
		if(StringUtil.isNotEmpty(customer.getStuName())){
			sb.append(" and s.stuName like '%"+customer.getStuName()+"%'");
		}
		if(StringUtil.isNotEmpty(customer.getSex())){
			sb.append(" and s.sex ='"+customer.getSex()+"'");
		}
		if(customer.getCompanyId()!=-1){
			sb.append(" and s.companyId ='"+customer.getCompanyId()+"'");
		}
		if(StringUtil.isNotEmpty(bbirthday)){
			sb.append(" and TO_DAYS(s.birthday)>=TO_DAYS('"+bbirthday+"')");
		}
		if(StringUtil.isNotEmpty(ebirthday)){
			sb.append(" and TO_DAYS(s.birthday)<=TO_DAYS('"+ebirthday+"')");
		}
		PreparedStatement pstmt=con.prepareStatement(sb.toString());
		ResultSet rs=pstmt.executeQuery();
		if(rs.next()){
			return rs.getInt("total");
		}else{
			return 0;
		}
	}
	
	public int customerDelete(Connection con,String delIds)throws Exception{
		String sql="delete from t_customer where stuId in("+delIds+")";
		PreparedStatement pstmt=con.prepareStatement(sql);
		return pstmt.executeUpdate();
	}
	
	public int customerAdd(Connection con,Customer customer)throws Exception{
		String sql="insert into t_customer values(null,?,?,?,?,?,?,?)";
		PreparedStatement pstmt=con.prepareStatement(sql);
		pstmt.setString(1, customer.getStuNo());
		pstmt.setString(2, customer.getStuName());
		pstmt.setString(3, customer.getSex());
		pstmt.setString(4, DateUtil.formatDate(customer.getBirthday(), "yyyy-MM-dd"));
		pstmt.setInt(5, customer.getCompanyId());
		pstmt.setString(6, customer.getEmail());
		pstmt.setString(7, customer.getStuDesc());
		return pstmt.executeUpdate();
	}
	
	public int customerModify(Connection con,Customer customer)throws Exception{
		String sql="update t_customer set stuNo=?,stuName=?,sex=?,birthday=?,companyId=?,email=?,stuDesc=? where stuId=?";
		PreparedStatement pstmt=con.prepareStatement(sql);
		pstmt.setString(1, customer.getStuNo());
		pstmt.setString(2, customer.getStuName());
		pstmt.setString(3, customer.getSex());
		pstmt.setString(4, DateUtil.formatDate(customer.getBirthday(), "yyyy-MM-dd"));
		pstmt.setInt(5, customer.getCompanyId());
		pstmt.setString(6, customer.getEmail());
		pstmt.setString(7, customer.getStuDesc());
		pstmt.setInt(8, customer.getStuId());
		return pstmt.executeUpdate();
	}
	
	public boolean getCustomerByCompanyId(Connection con,String companyId)throws Exception{
		String sql="select * from t_customer where companyId=?";
		PreparedStatement pstmt=con.prepareStatement(sql);
		pstmt.setString(1, companyId);
		ResultSet rs=pstmt.executeQuery();
		if(rs.next()){
			return true;
		}else{
			return false;
		}
	}
}
